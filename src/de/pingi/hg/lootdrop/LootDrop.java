package de.pingi.hg.lootdrop;

import java.util.ArrayList;
import java.util.Arrays;

public class LootDrop {

    private static ArrayList<LootDropItem> lootDropItems = new ArrayList<LootDropItem>(
            Arrays.asList(LootDropItem.DIAMOND_SWORD, LootDropItem.DIAMOND_HELMET, LootDropItem.DIAMOND_CHESTPLATE, LootDropItem.DIAMOND_LEGGINS, LootDropItem.DIAMOND_BOOTS,
                          LootDropItem.IRON_INGOT, LootDropItem.ANVIL, LootDropItem.ENCHANTER));


    public static ArrayList<LootDropItem> getLootDropItems() {
        return lootDropItems;
    }
}
