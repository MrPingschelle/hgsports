package de.pingi.hg.lootdrop;

import org.bukkit.Material;

public enum LootDropItem {

    DIAMOND_SWORD(Material.DIAMOND_SWORD, 1, 1, 6),
    DIAMOND_HELMET(Material.DIAMOND_HELMET,1, 1, 3),
    DIAMOND_CHESTPLATE(Material.DIAMOND_CHESTPLATE,1, 1, 3),
    DIAMOND_LEGGINS(Material.DIAMOND_LEGGINGS,1, 1, 3),
    DIAMOND_BOOTS(Material.DIAMOND_BOOTS,1, 1, 3),
    IRON_INGOT(Material.IRON_INGOT, 7, 15, 12),
    ANVIL(Material.ANVIL, 1, 1, 4),
    ENCHANTER(Material.ENCHANTMENT_TABLE, 1, 1, 4);


    private org.bukkit.Material item;
    private int minAmount;
    private int maxAmount;
    private int probability;

    LootDropItem(Material item, int minAmount, int maxAmount, int probability) {
        this.item = item;
        this.minAmount = minAmount;
        this.maxAmount = maxAmount;
        this.probability = probability;
    }

    public Material getItem() {
        return item;
    }

    public int getMinAmount() {
        return minAmount;
    }

    public int getMaxAmount() {
        return maxAmount;
    }

    public int getProbability() {
        return probability;
    }

}
