package de.pingi.hg.feast;

import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.Arrays;

public class Feast {

    private static ArrayList<Block> feastBlocks = new ArrayList<Block>();
    private static ArrayList<FeastItem> feastItems = new ArrayList<FeastItem>(
            Arrays.asList(FeastItem.DIAMOND_SWORD, FeastItem.DIAMOND_HELMET, FeastItem.DIAMOND_CHESTPLATE, FeastItem.DIAMOND_LEGGINS, FeastItem.DIAMOND_BOOTS,
                          FeastItem.BOW, FeastItem.ARROW, FeastItem.FNS, FeastItem.TNT, FeastItem.LAVA_BUCKET, FeastItem.WATER_BUCKET, FeastItem.SOUP, FeastItem.COBWEB,
                          FeastItem.COOKED_PORK, FeastItem.STEAK, FeastItem.COOKED_CHICKEN, FeastItem.CARROT));

    private static boolean hasSpawned = false;
    private static int announcementTime = 15*60*20;
    private static int spawnTime = 20*60*20;
    private static int x;
    private static int y;
    private static int z;


    public static int getAnnouncementTime() {
        return announcementTime;
    }

    public static int getSpawnTime() {
        return spawnTime;
    }

    public static int getX() {
        return x;
    }

    public static void setX(int x) {
        Feast.x = x;
    }

    public static int getY() {
        return y;
    }

    public static void setY(int y) {
        Feast.y = y;
    }

    public static int getZ() {
        return z;
    }

    public static void setZ(int z) {
        Feast.z = z;
    }

    public static ArrayList<Block> getFeastBlocks() {
        return feastBlocks;
    }

    public static void setFeastBlocks(ArrayList<Block> feastBlocks) {
        Feast.feastBlocks = feastBlocks;
    }

    public static boolean hasSpawned() {
        return hasSpawned;
    }

    public static void setHasSpawned(boolean hasSpawned) {
        Feast.hasSpawned = hasSpawned;
    }

    public static ArrayList<FeastItem> getFeastItems() {
        return feastItems;
    }

    public static void setFeastItems(ArrayList<FeastItem> feastItems) {
        Feast.feastItems = feastItems;
    }
}
