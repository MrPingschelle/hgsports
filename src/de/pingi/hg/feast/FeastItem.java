package de.pingi.hg.feast;

import org.bukkit.Material;

public enum FeastItem {

    DIAMOND_SWORD(Material.DIAMOND_SWORD, 1, 1, 3),
    DIAMOND_HELMET(Material.DIAMOND_HELMET,1, 1, 3),
    DIAMOND_CHESTPLATE(Material.DIAMOND_CHESTPLATE,1, 1, 3),
    DIAMOND_LEGGINS(Material.DIAMOND_LEGGINGS,1, 1, 3),
    DIAMOND_BOOTS(Material.DIAMOND_BOOTS,1, 1, 3),
    BOW(Material.BOW, 1, 1, 4),
    ARROW(Material.ARROW, 5, 7, 7),
    FNS(Material.FLINT_AND_STEEL, 1, 1, 5),
    TNT(Material.TNT, 2, 4, 5),
    LAVA_BUCKET(Material.LAVA_BUCKET, 1, 1, 5),
    WATER_BUCKET(Material.WATER_BUCKET, 1, 1, 5),
    SOUP(Material.MUSHROOM_SOUP, 3, 7, 8),
    COBWEB(Material.WEB, 1, 6, 5),
    COOKED_PORK(Material.GRILLED_PORK, 3, 8, 12),
    STEAK(Material.COOKED_BEEF, 3, 8, 12),
    COOKED_CHICKEN(Material.COOKED_CHICKEN, 3, 8, 12),
    CARROT(Material.CARROT, 5, 10, 12);

    private org.bukkit.Material item;
    private int minAmount;
    private int maxAmount;
    private int probability;

    FeastItem(Material item, int minAmount, int maxAmount, int probability) {
        this.item = item;
        this.minAmount = minAmount;
        this.maxAmount = maxAmount;
        this.probability = probability;
    }

    public Material getItem() {
        return item;
    }

    public int getMinAmount() {
        return minAmount;
    }

    public int getMaxAmount() {
        return maxAmount;
    }

    public int getProbability() {
        return probability;
    }

}
