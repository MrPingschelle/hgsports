package de.pingi.hg.kit.mainkits;

import de.pingi.hg.PlayerManager;
import de.pingi.hg.kit.Kit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

public class Fireman extends Kit {

    public Fireman() {
        super("Fireman", Material.WATER_BUCKET, 0);
    }

    @EventHandler
    public void onFireDamage (EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            Player player = (Player) e.getEntity();
            if (PlayerManager.getMainKits().containsKey(player)) {
                if (PlayerManager.getMainKits().get(player) == this) {
                    if (e.getCause().equals(EntityDamageEvent.DamageCause.FIRE) || e.getCause().equals(EntityDamageEvent.DamageCause.FIRE_TICK) || e.getCause().equals(EntityDamageEvent.DamageCause.LAVA) || e.getCause().equals(EntityDamageEvent.DamageCause.LIGHTNING)) {
                        e.setCancelled(true);
                    }
                }
            }
        }
    }

    @Override
    public void equip(Player player, int slot) {
        player.getInventory().setItem(slot, new ItemStack(Material.WATER_BUCKET));
    }
}
