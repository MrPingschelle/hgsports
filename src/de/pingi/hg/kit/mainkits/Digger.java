package de.pingi.hg.kit.mainkits;

import de.pingi.hg.kit.Kit;
import de.pingi.util.StructuresUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.event.Event;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class Digger extends Kit {

    public Digger() {
        super("Digger", Material.STONE_SPADE, 30*20);
        this.setClickItem(new ItemStack(Material.STONE_SPADE), "Dig hole");
    }

    @Override
    public void onClickUse(Event e) {
        if (isUsable(e)) {
            PlayerInteractEvent event = (PlayerInteractEvent) e;
            Block clickedBlock = event.getClickedBlock();
            Block block1 = Bukkit.getWorld("world").getBlockAt(clickedBlock.getX() - 2, clickedBlock.getY() - 5, clickedBlock.getZ() - 2);
            Block block2 = Bukkit.getWorld("world").getBlockAt(clickedBlock.getX() + 2, clickedBlock.getY(), clickedBlock.getZ() + 2);
            StructuresUtil.spawnCuboid(block1, block2, Material.AIR);
            Bukkit.getWorld("world").playSound(clickedBlock.getLocation(), Sound.DIG_STONE, 10, 1);
        }
    }


    @Override
    public boolean isUsable(Event e) {
        return e instanceof PlayerInteractEvent && ((PlayerInteractEvent) e).getAction().equals(Action.RIGHT_CLICK_BLOCK) && !((PlayerInteractEvent) e).getAction().equals(Action.RIGHT_CLICK_AIR);
    }
}
