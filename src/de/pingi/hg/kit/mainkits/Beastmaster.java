package de.pingi.hg.kit.mainkits;

import de.pingi.hg.kit.Kit;
import de.pingi.main.Main;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class Beastmaster extends Kit {

    public Beastmaster() {
        super("Beastmaster", Material.BONE, 60*20);
        this.setClickItem(new ItemStack(Material.BONE), "Summon wolves");
    }

    @Override
    public void onClickUse(Event e) {
        if (isUsable(e)) {
            PlayerInteractEvent event = (PlayerInteractEvent) e;
            this.spawnWolf(event.getPlayer());
            this.spawnWolf(event.getPlayer());
            this.spawnWolf(event.getPlayer());
        }
    }


    public void spawnWolf(Player player) {
        Wolf wolf = (Wolf) player.getWorld().spawnEntity(player.getLocation(), EntityType.WOLF);
        wolf.setOwner(player);
    }

    @Override
    public boolean isUsable(Event e) {
        return e instanceof PlayerInteractEvent;
    }
}
