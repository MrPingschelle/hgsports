package de.pingi.hg.kit.mainkits;

import de.pingi.hg.PlayerManager;
import de.pingi.hg.kit.Kit;
import de.pingi.listener.GameTimeEvent;
import de.pingi.main.Main;
import de.pingi.util.ColorUtil;
import de.pingi.util.Math;
import de.pingi.util.MessageType;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.security.Key;
import java.util.HashMap;
import java.util.Map;

public class Snorlax extends Kit {

    private HashMap<Player, Integer> eatenSoups = new HashMap<Player, Integer>();
    private HashMap<Player, Boolean> onSnorlaxEffect = new HashMap<Player, Boolean>();
    private HashMap<Player, Integer> timeLeft = new HashMap<Player, Integer>();

    public Snorlax() {
        super("Snorlax", Material.BOWL, 0);
    }

    @EventHandler
    public void onSnorlaxMachtSachen(PlayerInteractEvent e) {
        if (PlayerManager.getMainKits().containsKey(e.getPlayer())) {
            if (PlayerManager.getMainKits().get(e.getPlayer()) == this) {
                if (e.getItem().getType() == Material.MUSHROOM_SOUP && (e.getPlayer().getHealth() < 20 || e.getPlayer().getFoodLevel() < 20) && (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))) {
                    if (!onSnorlaxEffect.getOrDefault(e.getPlayer(), false)) {
                        eatenSoups.put(e.getPlayer(), eatenSoups.getOrDefault(e.getPlayer(), 0) + 1);
                        if (eatenSoups.getOrDefault(e.getPlayer(), 0) == 32) {
                            eatenSoups.put(e.getPlayer(), 0);
                            onSnorlaxEffect.put(e.getPlayer(), true);
                            timeLeft.put(e.getPlayer(), 15*20);
                            e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 15*20, 1));
                            e.getPlayer().sendMessage(ColorUtil.encolor("Your stomach has transformed your eaten soups into resistance!", MessageType.GOOD));
                        }
                    }
                    else if(onSnorlaxEffect.getOrDefault(e.getPlayer(), false)) {
                        if (timeLeft.getOrDefault(e.getPlayer(), 0) <= 4*20) {
                            timeLeft.put(e.getPlayer(), timeLeft.getOrDefault(e.getPlayer(), 0) + 8*20);
                            int currentDuration = 0;
                            for (PotionEffect potionEffect : e.getPlayer().getActivePotionEffects()) {
                                if (potionEffect.getType().equals(PotionEffectType.DAMAGE_RESISTANCE)) {
                                    currentDuration = potionEffect.getDuration();
                                }
                            }
                            e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, currentDuration + 8*20, 1), true);
                            e.getPlayer().sendMessage(ColorUtil.encolor("+", MessageType.GOOD));
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onGameTimeChange(GameTimeEvent e) {
        for (Player player : timeLeft.keySet()) {
            if (timeLeft.getOrDefault(player, 0) >= 20) {
                timeLeft.put(player, timeLeft.get(player) - 20);
            }
            if (timeLeft.getOrDefault(player, 0) == 0) {
                onSnorlaxEffect.put(player, false);
            }
        }
    }

}
