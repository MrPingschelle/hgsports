package de.pingi.hg.kit;

import de.pingi.hg.GameState;
import de.pingi.hg.PlayerManager;
import de.pingi.hg.Timer;
import de.pingi.hg.kit.mainkits.Beastmaster;
import de.pingi.hg.kit.mainkits.Digger;
import de.pingi.hg.kit.mainkits.Fireman;
import de.pingi.hg.kit.mainkits.Snorlax;
import de.pingi.hg.kit.sidekits.Flora;
import de.pingi.hg.kit.sidekits.*;
import de.pingi.util.ColorUtil;
import de.pingi.util.MessageType;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.Arrays;

public class KitManager implements Listener {

    private static ArrayList<Kit> mainKits = new ArrayList<Kit>(Arrays.asList(new Beastmaster(), new Digger(), new Fireman(), new Snorlax()));
    private static ArrayList<Kit> sideKits = new ArrayList<Kit>(Arrays.asList(new Absorber(), new Burrower(), new Flora(), new Hobbit(), new Holefighter(), new Lifedonor(), new Mirror(), new Oasis()));
    private static ArrayList<Kit> kits = new ArrayList<Kit>(Arrays.asList(new Beastmaster(), new Digger(), new Fireman(), new Snorlax(),
                                                                          new Absorber(), new Burrower(), new Flora(), new Hobbit(), new Holefighter(), new Lifedonor(), new Mirror(), new Oasis()));

    //@EventHandler
    public void onKitChestOpen(PlayerInteractEvent e) {
        if ((e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) && Timer.getGameState() == GameState.PREGAME) {
            if (e.getPlayer().getItemInHand().getType() == Material.CHEST) {
                openMainKitInv(e.getPlayer());
            }
            if (e.getPlayer().getItemInHand().getType() == Material.ENDER_CHEST) {
                openSideKitInv(e.getPlayer());
            }
        }
    }

    public void openMainKitInv(Player player) {
        Inventory kitInv = Bukkit.createInventory(null, 9*6, ColorUtil.accentuate("Main Kits","", MessageType.NEUTRAL));
        for (Kit kit : mainKits) {
            kitInv.addItem(kit.getItem());
        }
        player.openInventory(kitInv);
    }

    public void openSideKitInv(Player player) {
        Inventory kitInv = Bukkit.createInventory(null, 9*6, ColorUtil.accentuate("Side Kits","", MessageType.NEUTRAL));
        for (Kit kit : sideKits) {
            kitInv.addItem(kit.getItem());
        }
        player.openInventory(kitInv);
    }

    //@EventHandler
    public void onClick(InventoryClickEvent e) {
            if (e.getClickedInventory() != null && e.getCurrentItem() != null && Timer.getGameState() == GameState.PREGAME) {
                if (e.getClickedInventory().getTitle().equals(ColorUtil.accentuate("Main Kits", "", MessageType.NEUTRAL))) {
                    Player p = (Player) e.getWhoClicked();
                    Kit kit = getKitByName(e.getCurrentItem().getItemMeta().getDisplayName());
                    PlayerManager.getMainKits().put(p, kit);
                    p.closeInventory();
                }
                if (e.getClickedInventory().getTitle().equals(ColorUtil.accentuate("Side Kits", "", MessageType.NEUTRAL))) {
                    Player p = (Player) e.getWhoClicked();
                    Kit kit = getKitByName(e.getCurrentItem().getItemMeta().getDisplayName());
                    PlayerManager.getSideKits().put(p, kit);
                    p.closeInventory();
                }
            }
    }

    public Kit getKitByName(String name) {
        for (Kit kit : kits) {
            if (kit.getName().equalsIgnoreCase(name)) {
                return kit;
            }
        }
        return null;
    }

    public static ArrayList<Kit> getKits() {
        return kits;
    }

    public static ArrayList<Kit> getMainKits() {
        return mainKits;
    }

    public static ArrayList<Kit> getSideKits() {
        return sideKits;
    }
}
