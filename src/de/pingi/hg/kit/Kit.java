package de.pingi.hg.kit;

import de.pingi.hg.PlayerManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Kit implements Listener {

    private String name = "";
    private Material material;
    private int cooldown;
    private ItemStack clickItem = null;
    private boolean usable;

    public Kit(String name, Material material, int cooldown) {
        this.name = name;
        this.material = material;
        this.cooldown = cooldown;
    }

    public void onClickUse(Event e) { }

    public void equip(Player player, int slot) {
        player.getInventory().setItem(slot, this.getClickItem());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCooldown() {
        return cooldown;
    }

    public void setCooldown(int cooldown) {
        this.cooldown = cooldown;
    }

    public ItemStack getClickItem() {
        return clickItem;
    }

    public void setClickItem(ItemStack clickItem, String name) {
        this.clickItem = clickItem;
        clickItem.getItemMeta().setDisplayName(name);
    }

    public ItemStack getItem() {
        ItemStack itemStack = new ItemStack(material);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(name);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public boolean isUsable(Event e) {
        return usable;
    }

    public void setUsable(boolean usable) {
        this.usable = usable;
    }
}
