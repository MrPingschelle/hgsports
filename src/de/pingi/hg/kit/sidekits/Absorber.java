package de.pingi.hg.kit.sidekits;

import de.pingi.hg.PlayerManager;
import de.pingi.hg.Timer;
import de.pingi.hg.kit.Kit;
import de.pingi.util.ColorUtil;
import de.pingi.util.MessageType;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.HashMap;

public class Absorber extends Kit {

    private HashMap<Player, Double> absorbFactors = new HashMap<Player, Double>();
    private HashMap<Player, Player> lastEnemies = new HashMap<Player, Player>();
    private HashMap<Player, Integer> lastHitTimes = new HashMap<Player, Integer>();

    public Absorber() {
        super("Absorber", Material.DEAD_BUSH, 0);
    }

    @EventHandler
    public void onAbsorberMorber(EntityDamageByEntityEvent e) {
        if (e.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_ATTACK)) {
            if (e.getDamager() instanceof Player) {
                Player damager = (Player) e.getDamager();
                if (e.getEntity() instanceof Player) {
                    Player damaged = (Player) e.getEntity();
                    if (PlayerManager.getSideKits().containsKey(damaged)) {
                        if (PlayerManager.getSideKits().get(damaged) == this) {
                            int playerCount = 0;
                            for (Entity entity : damaged.getNearbyEntities(10, 10, 10)) {
                                if (entity instanceof Player) {
                                    playerCount += 1;
                                }
                            }
                            if (playerCount >= 2) {
                                e.setDamage(e.getFinalDamage() * absorbFactors.getOrDefault(damaged, 1.0));
                                if (lastEnemies.getOrDefault(damaged, null) != damager) {
                                    if ((Timer.getGameTime() - lastHitTimes.getOrDefault(damaged, Timer.getGameTime())) <= 10*20) {
                                        absorbFactors.put(damaged, absorbFactors.getOrDefault(damaged, 1.0) * 0.99);
                                        damager.sendMessage(ColorUtil.encolor("Your opponent has absorbed your hit.", MessageType.BAD));
                                        damaged.sendMessage(ColorUtil.encolor("You absorbed your opponent's hit!", MessageType.GOOD));
                                    }
                                    else {
                                        damaged.sendMessage(ColorUtil.encolor("You absorption powers left you.", MessageType.BAD));
                                        absorbFactors.put(damaged, 1.0);
                                    }
                                }
                            }
                            else {
                                if (absorbFactors.getOrDefault(damaged, 1.0) < 1) {
                                    damaged.sendMessage(ColorUtil.encolor("Your absorption powers only work in dangerous situations.", MessageType.BAD));
                                }
                                absorbFactors.put(damaged, 1.0);
                            }
                            lastEnemies.put(damaged, damager);
                            lastHitTimes.put(damaged, Timer.getGameTime());
                        }
                    }
                }
            }
        }
    }

}
