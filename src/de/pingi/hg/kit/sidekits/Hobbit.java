package de.pingi.hg.kit.sidekits;

import de.pingi.hg.kit.Kit;
import de.pingi.main.Main;
import de.pingi.util.ColorUtil;
import de.pingi.util.MessageType;
import de.pingi.util.StructuresUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.Map;

public class Hobbit extends Kit {

    public Hobbit() {
        super("Hobbit", Material.TORCH, 60*20);
        this.setClickItem(new ItemStack(Material.TORCH), "Create chamber");
    }

    @Override
    public void onClickUse(Event e) {
        if (isUsable(e)) {
            PlayerInteractEvent event = (PlayerInteractEvent) e;

            HashMap<Player, Double> distances = new HashMap<Player, Double>();

            for (Entity entity : event.getPlayer().getNearbyEntities(10, 10, 10)) {
                if (entity instanceof Player) {
                    Player player = (Player) entity;
                    double distance = event.getPlayer().getLocation().distance(player.getLocation());
                    distances.put(player, distance);
                }
            }

            Map.Entry<Player, Double> minEntry = null;

            for (Map.Entry<Player, Double> entry : distances.entrySet())
            {
                assert false;
                if (minEntry == null  || minEntry.getValue() > entry.getValue())
                {
                    minEntry = entry;
                }
            }

            assert false;
            Player closestPlayer = minEntry.getKey();

            for (Entity entity : event.getPlayer().getNearbyEntities(10, 10, 10)) {
                if (entity instanceof Player) {
                    Player player = (Player) entity;
                    if (player != event.getPlayer() && player != closestPlayer) {
                        if (distances.get(player) <= 5) {
                            player.teleport(new Location(event.getPlayer().getWorld(), event.getPlayer().getLocation().getX(), event.getPlayer().getLocation().getY() + 5, event.getPlayer().getLocation().getZ()));
                        }
                    }
                }
            }

            Location location = new Location(event.getPlayer().getWorld(), event.getPlayer().getLocation().getX(), event.getPlayer().getLocation().getY() - 1, event.getPlayer().getLocation().getZ());
            StructuresUtil.spawnCylinder(location, 5, 6, Material.COBBLESTONE);
            StructuresUtil.spawnCylinder(new Location(location.getWorld(), location.getX(), location.getY() + 1, location.getZ()), 3, 3, Material.AIR);
            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
                @Override
                public void run() {
                    StructuresUtil.spawnCylinder(location, 5, 6, Material.AIR);
                }
            }, 30*20);
        }
    }

    @Override
    public boolean isUsable(Event e) {
        if (e instanceof PlayerInteractEvent) {
            PlayerInteractEvent event = (PlayerInteractEvent) e;
            if (event.getPlayer().getLocation().getY() >= 50 && event.getPlayer().getLocation().getY() <= 80) {
                int playerCount = 0;
                for (Entity entity : event.getPlayer().getNearbyEntities(10, 10, 10)) {
                    if (entity instanceof Player) {
                        playerCount += 1;
                    }
                }
                if (playerCount >= 2) {
                    return true;
                } else {
                    event.getPlayer().sendMessage(ColorUtil.encolor("You can only use this kit when two or more players are nearby.", MessageType.BAD));
                    return false;
                }
            }
            else {
                event.getPlayer().sendMessage(ColorUtil.encolor("You have to be between y: 50 and y: 80 to use this kit.", MessageType.BAD));
                return false;
            }
        }
        else {
            return false;
        }
    }

}
