package de.pingi.hg.kit.sidekits;

import de.pingi.hg.PlayerManager;
import de.pingi.hg.kit.Kit;
import de.pingi.main.Main;
import de.pingi.util.ColorUtil;
import de.pingi.util.MessageType;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class Selector extends Kit {

    private HashMap<Player, Player> selectedPlayers = new HashMap<Player, Player>();

    public Selector() {
        super("Selector", Material.IRON_SWORD, 60*20);
        setClickItem(new ItemStack(Material.IRON_FENCE), "Select player");
    }

    @EventHandler
    public void onPlayerDamageByPlayer(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player) {
            Player damaged = (Player) e.getEntity();
            if (e.getDamager() instanceof Player) {
                Player damager = (Player) e.getDamager();
                if (!(selectedPlayers.containsKey(damager) || selectedPlayers.containsValue(damager)) && (selectedPlayers.containsKey(damaged) || selectedPlayers.containsValue(damaged))) {
                    damager.sendMessage(ColorUtil.encolor("That player is in a private duel.", MessageType.BAD));
                    e.setCancelled(true);
                }
                if ((selectedPlayers.containsKey(damager) || selectedPlayers.containsValue(damager)) && !(selectedPlayers.containsKey(damaged) || selectedPlayers.containsValue(damaged))) {
                    damager.sendMessage(ColorUtil.encolor("You are in a private duel.", MessageType.BAD));
                    e.setCancelled(true);

                }


            }
        }
    }

    @Override
    public void onClickUse(Event e) {
        if (isUsable(e)) {
            EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
            selectedPlayers.put((Player) event.getDamager(), (Player) event.getEntity());
            ((Player) event.getDamager()).sendMessage(ColorUtil.encolor("You have selected ", MessageType.GOOD) + ColorUtil.accentuate(((Player) event.getEntity()).getDisplayName() + ".","", MessageType.GOOD));
            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
                @Override
                public void run() {
                   selectedPlayers.remove((Player) event.getDamager());
                }
            },15*20);
        }
    }

    @Override
    public boolean isUsable(Event e) {
        if (e instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
            if (event.getDamager() instanceof Player) {
                Player damager = (Player) event.getDamager();
                if (event.getEntity() instanceof Player) {
                    if (event.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_ATTACK)) {
                        Player damaged = (Player) event.getEntity();
                        int playerCount = 0;
                        for (Entity entity : damager.getNearbyEntities(10, 10, 10)) {
                            if (entity instanceof Player) {
                                playerCount += 1;
                            }
                        }
                        if (playerCount >= 2) {
                            event.setCancelled(true);
                            return true;
                        } else {
                            damager.sendMessage(ColorUtil.encolor("You can only use this kit when two or more players are nearby.", MessageType.BAD));
                            return false;
                        }
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
