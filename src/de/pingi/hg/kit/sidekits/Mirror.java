package de.pingi.hg.kit.sidekits;

import de.pingi.hg.kit.Kit;
import de.pingi.main.Main;
import de.pingi.util.ColorUtil;
import de.pingi.util.EquipUtil;
import de.pingi.util.MessageType;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

public class Mirror extends Kit {

    public Mirror() {
        super("Mirror", Material.THIN_GLASS, 45*20);
        this.setClickItem(new ItemStack(Material.THIN_GLASS), "Mirror your opponent's armor");
    }

    @Override
    public void onClickUse(Event e) {
        if (isUsable(e)) {
            EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
            Player damager = (Player) event.getDamager();
            Player damaged = (Player) event.getEntity();
            ItemStack[] oldArmorContents = damager.getInventory().getArmorContents();
            damager.getInventory().setArmorContents(damaged.getInventory().getArmorContents());
            damager.sendMessage(ColorUtil.encolor("You have mirrored your opponent's armor!", MessageType.GOOD));
            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
                @Override
                public void run() {
                    damager.getInventory().setArmorContents(oldArmorContents);
                    damager.sendMessage(ColorUtil.encolor("Your armor has been reset", MessageType.BAD));
                }
            }, 15*20);
        }
    }

    @Override
    public boolean isUsable(Event e) {
        if (e instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
            if (event.getDamager() instanceof Player) {
                Player damager = (Player) event.getDamager();
                if (event.getEntity() instanceof Player) {
                    Player damaged = (Player) event.getEntity();
                    if (event.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_ATTACK)) {
                        Bukkit.broadcastMessage("" + EquipUtil.calcEP(damaged));
                        Bukkit.broadcastMessage("" + EquipUtil.calcEP(damager));
                        if (EquipUtil.calcEP(damaged) > EquipUtil.calcEP(damager)) {
                            return true;
                        }
                        else {
                            damager.sendMessage(ColorUtil.encolor("You can only mirror the armor of weaker players.", MessageType.BAD));
                            return false;
                        }
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
