package de.pingi.hg.kit.sidekits;

import de.pingi.hg.PlayerManager;
import de.pingi.hg.kit.Kit;
import de.pingi.main.Main;
import de.pingi.util.ColorUtil;
import de.pingi.util.MessageType;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class Holefighter extends Kit {

    public Holefighter() {
        super("Holefighter", Material.WOOD_SPADE, 45 * 20);
        this.setClickItem(new ItemStack(Material.WOOD_SPADE), "Capture player");
    }

    @Override
    public void onClickUse(Event e) {
        if (isUsable(e)) {
            EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
            Player clickedPlayer = (Player) event.getEntity();
            double x = clickedPlayer.getLocation().getX();
            double y = clickedPlayer.getLocation().getY();
            double z = clickedPlayer.getLocation().getZ();
            Bukkit.getServer().getWorld("world").getBlockAt(new Location(clickedPlayer.getWorld(), x, y - 1, z)).setType(Material.AIR);
            Bukkit.getServer().getWorld("world").getBlockAt(new Location(clickedPlayer.getWorld(), x, y - 2, z)).setType(Material.AIR);
            Bukkit.getServer().getWorld("world").getBlockAt(new Location(clickedPlayer.getWorld(), x, y - 3, z)).setType(Material.AIR);
            clickedPlayer.teleport(new Location(clickedPlayer.getWorld(), x, y - 2, z));
        }
    }

    @Override
    public boolean isUsable(Event e) {
        if (e instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
            if (event.getDamager() instanceof Player) {
                Player damager = (Player) event.getDamager();
                if (event.getEntity() instanceof Player) {
                    if (event.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_ATTACK)) {
                        Player damaged = (Player) event.getEntity();
                        int playerCount = 0;
                        for (Entity entity : damager.getNearbyEntities(10, 10, 10)) {
                            if (entity instanceof Player) {
                                playerCount += 1;
                            }
                        }
                        if (playerCount >= 2) {
                            event.setCancelled(true);
                            return true;
                        } else {
                            damager.sendMessage(ColorUtil.encolor("You can only use this kit when two or more players are nearby.", MessageType.BAD));
                            return false;
                        }
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
