package de.pingi.hg.kit.sidekits;

import de.pingi.hg.PlayerManager;
import de.pingi.hg.kit.Kit;
import de.pingi.main.Main;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.Inventory;

public class Flora extends Kit {

    public Flora() {
        super("Flora", Material.RED_ROSE, 0);
    }

    @EventHandler
    public void onFlowerPower(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player) {
            Player player = (Player) e.getEntity();
            if (PlayerManager.getSideKits().containsKey(player)) {
                if (PlayerManager.getSideKits().get(player) == this) {
                    if (e.getDamager() instanceof Player) {
                        Player damager = (Player) e.getDamager();
                        Inventory damagerInv = damager.getInventory();
                        if (damagerInv.contains(Material.YELLOW_FLOWER) || damagerInv.contains(Material.RED_ROSE)) {
                            e.setCancelled(true);
                        }
                    }
                }
            }
        }
    }

}
