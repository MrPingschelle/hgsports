package de.pingi.hg.kit.sidekits;

import de.pingi.hg.kit.Kit;
import de.pingi.util.ColorUtil;
import de.pingi.util.MessageType;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Lifedonor extends Kit {

    public Lifedonor() {
        super("Lifedonor", Material.APPLE, 60*20);
        this.setClickItem(new ItemStack(Material.APPLE), "Support player");
    }

    @Override
    public void onClickUse(Event e) {
        if (isUsable(e)) {
            EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
            Player damaged = (Player) event.getEntity();
            Player damager = (Player) event.getDamager();
            damaged.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 30*20, 0));
            damager.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 30*20, -1));
        }
    }

    @Override
    public boolean isUsable(Event e) {
        if (e instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
            if (event.getDamager() instanceof Player) {
                Player damager = (Player) event.getDamager();
                if (event.getEntity() instanceof Player) {
                    if (event.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_ATTACK)) {
                        Player damaged = (Player) event.getEntity();
                        int playerCount = 0;
                        for (Entity entity : damager.getNearbyEntities(10, 10, 10)) {
                            if (entity instanceof Player) {
                                playerCount += 1;
                            }
                        }
                        if (playerCount >= 2) {
                            event.setCancelled(true);
                            return true;
                        } else {
                            damager.sendMessage(ColorUtil.encolor("You can only use this kit when two or more players are nearby.", MessageType.BAD));
                            return false;
                        }
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
