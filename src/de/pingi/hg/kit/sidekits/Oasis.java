package de.pingi.hg.kit.sidekits;

import de.pingi.hg.PlayerManager;
import de.pingi.hg.kit.Kit;
import de.pingi.main.Main;
import de.pingi.util.ColorUtil;
import de.pingi.util.MessageType;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class Oasis extends Kit {

    private HashMap<Player, Integer> oasisTaskIDs = new HashMap<Player, Integer>();

    public Oasis() {
        super("Oasis", Material.WATER_LILY, 0);
    }

    @EventHandler
    public void onOasisEnter(PlayerMoveEvent e) {
        if (PlayerManager.getSideKits().containsKey(e.getPlayer())) {
            if (PlayerManager.getSideKits().get(e.getPlayer()) == this) {
                if (e.getPlayer().getLocation().getBlock().getType() == Material.WATER || e.getPlayer().getLocation().getBlock().getType() == Material.STATIONARY_WATER) {
                    if (oasisTaskIDs.getOrDefault(e.getPlayer(), null) == null){
                        oasisTaskIDs.put(e.getPlayer(), Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {
                            @Override
                            public void run() {
                                e.getPlayer().getInventory().addItem(new ItemStack(Material.BROWN_MUSHROOM));
                                e.getPlayer().getInventory().addItem(new ItemStack(Material.RED_MUSHROOM));
                                e.getPlayer().sendMessage(ColorUtil.encolor("boi u fuckin rich", MessageType.GOOD));
                                e.getPlayer().sendMessage(ColorUtil.encolor("girl u fuckin rich", MessageType.GOOD));
                                e.getPlayer().sendMessage(ColorUtil.encolor("crymonota", MessageType.GOOD));
                            }
                        }, 3 * 20, 3 * 20));
                    }
                }
            }
        }
    }

    @EventHandler
    public void onOasisLeave(PlayerMoveEvent e) {
        if (PlayerManager.getSideKits().containsKey(e.getPlayer())) {
            if (PlayerManager.getSideKits().get(e.getPlayer()) == this) {
                if (!(e.getPlayer().getLocation().getBlock().getType() == Material.WATER || e.getPlayer().getLocation().getBlock().getType() == Material.STATIONARY_WATER)) {
                    if (oasisTaskIDs.getOrDefault(e.getPlayer(), null) != null) {
                        Bukkit.getScheduler().cancelTask(oasisTaskIDs.get(e.getPlayer()));
                        oasisTaskIDs.remove(e.getPlayer());
                    }
                }
            }
        }
    }

}
