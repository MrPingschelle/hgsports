package de.pingi.hg.kit.sidekits;

import de.pingi.hg.kit.Kit;
import de.pingi.util.ColorUtil;
import de.pingi.util.MessageType;
import de.pingi.util.StructuresUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class Burrower extends Kit {

    public Burrower() {
        super("Burrower", Material.IRON_SPADE, 60*20);
        this.setClickItem(new ItemStack(Material.IRON_SPADE), "Escape");
    }

    @Override
    public void onClickUse(Event e) {
        if (isUsable(e)) {
            PlayerInteractEvent event = (PlayerInteractEvent) e;
            Block block1 = Bukkit.getWorld("world").getBlockAt(event.getPlayer().getLocation().getBlockX() - 3, event.getPlayer().getLocation().getBlockY() - 31, event.getPlayer().getLocation().getBlockZ() - 3);
            Block block2 = Bukkit.getWorld("world").getBlockAt(event.getPlayer().getLocation().getBlockX() + 3, event.getPlayer().getLocation().getBlockY() - 24, event.getPlayer().getLocation().getBlockZ() + 3);
            Block block3 = Bukkit.getWorld("world").getBlockAt(event.getPlayer().getLocation().getBlockX() - 2, event.getPlayer().getLocation().getBlockY() - 30, event.getPlayer().getLocation().getBlockZ() - 2);
            Block block4 = Bukkit.getWorld("world").getBlockAt(event.getPlayer().getLocation().getBlockX() + 2, event.getPlayer().getLocation().getBlockY() - 25, event.getPlayer().getLocation().getBlockZ() + 2);
            StructuresUtil.spawnCuboid(block1, block2, Material.COBBLESTONE);
            StructuresUtil.spawnCuboid(block3, block4, Material.AIR);
            event.getPlayer().teleport(new Location(event.getPlayer().getWorld(), block3.getX() + 2, block3.getY(), block3.getZ() + 3));
        }
    }

    @Override
    public boolean isUsable(Event e) {
        if (e instanceof PlayerInteractEvent) {
            PlayerInteractEvent event = (PlayerInteractEvent) e;
            if (event.getPlayer().getLocation().getY() >= 50 && event.getPlayer().getLocation().getY() <= 80) {
                int playerCount = 0;
                for (Entity entity : event.getPlayer().getNearbyEntities(10, 10, 10)) {
                    if (entity instanceof Player) {
                        playerCount += 1;
                    }
                }
                if (playerCount >= 2) {
                    return true;
                } else {
                    event.getPlayer().sendMessage(ColorUtil.encolor("You can only use this kit when two or more players are nearby.", MessageType.BAD));
                    return false;
                }
            }
            else {
                event.getPlayer().sendMessage(ColorUtil.encolor("You have to be between y: 50 and y: 80 to use this kit.", MessageType.BAD));
                return false;
            }
        }
        else {
            return false;
        }
    }
}
