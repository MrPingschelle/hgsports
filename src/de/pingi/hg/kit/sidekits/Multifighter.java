package de.pingi.hg.kit.sidekits;

import de.pingi.hg.PlayerManager;
import de.pingi.hg.kit.Kit;
import de.pingi.util.ColorUtil;
import de.pingi.util.MessageType;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class Multifighter extends Kit {

    public Multifighter() {
        super("Multifighter", Material.BOOK_AND_QUILL, 0);
    }

    @EventHandler
    public void onMultiFight(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player) {
            Player damaged = (Player) e.getEntity();
            if (e.getDamager() instanceof Player) {
                Player damager = (Player) e.getDamager();
                if (PlayerManager.getSideKits().containsKey(damager)) {
                    if (PlayerManager.getSideKits().get(damager) == this) {
                        int playerCount = 0;
                        for (Entity entity : damager.getNearbyEntities(10, 10, 10)) {
                            if (entity instanceof Player) {
                                playerCount += 1;
                            }
                        }
                        if (playerCount >= 2) {
                            for (Entity entity : damager.getNearbyEntities(10, 10, 10)) {
                                if (entity instanceof Player) {
                                    if (entity != damaged) {
                                        ((Player) entity).damage(e.getFinalDamage());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
