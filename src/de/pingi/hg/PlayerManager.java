package de.pingi.hg;

import de.pingi.hg.kit.Kit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

public class PlayerManager {

    private static HashMap<Player, Player> closestPlayers = new HashMap<Player, Player>();

    private static HashMap<Player, ArrayList<Player>> killedPlayers = new HashMap<Player, ArrayList<Player>>();
    private static HashMap<Player, Integer> kills = new HashMap<Player, Integer>();

    private static HashMap<Player, Kit> mainKits = new HashMap<Player, Kit>();
    private static HashMap<Player, Kit> sideKits = new HashMap<Player, Kit>();

    private static HashMap<Player, Integer> mainKitCooldowns = new HashMap<Player, Integer>();
    private static HashMap<Player, Integer> sideKitCooldowns = new HashMap<Player, Integer>();

    private static HashMap<Player, Boolean> onMainKitCooldowns = new HashMap<Player, Boolean>();
    private static HashMap<Player, Boolean> onSideKitCooldowns = new HashMap<Player, Boolean>();

    private static HashMap<Player, Integer> sideKitTaskIDs = new HashMap<Player, Integer>();
    private static HashMap<Player, Integer> mainKitTaskIDs = new HashMap<Player, Integer>();

    private static HashMap<Player, Boolean> aliveOrDeadPlayers = new HashMap<Player, Boolean>();

    public static HashMap<Player, Player> getClosestPlayers() {
        return closestPlayers;
    }

    public static HashMap<Player, ArrayList<Player>> getKilledPlayers() {
        return killedPlayers;
    }

    public static HashMap<Player, Integer> getKills() {
        return kills;
    }

    public static HashMap<Player, Kit> getMainKits() {
        return mainKits;
    }

    public static HashMap<Player, Kit> getSideKits() {
        return sideKits;
    }

    public static HashMap<Player, Boolean> getOnMainKitCooldowns() {
        return onMainKitCooldowns;
    }

    public static HashMap<Player, Boolean> getOnSideKitCooldowns() {
        return onSideKitCooldowns;
    }

    public static HashMap<Player, Integer> getMainKitCooldowns() {
        return mainKitCooldowns;
    }

    public static HashMap<Player, Integer> getSideKitCooldowns() {
        return sideKitCooldowns;
    }

    public static HashMap<Player, Integer> getMainKitTaskIDs() {
        return mainKitTaskIDs;
    }

    public static HashMap<Player, Integer> getSideKitTaskIDs() {
        return sideKitTaskIDs;
    }

    public static HashMap<Player, Boolean> getAliveOrDeadPlayers() {
        return aliveOrDeadPlayers;
    }
}
