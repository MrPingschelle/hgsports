package de.pingi.hg;

public enum GameState {

    PREGAME(-3 * 60 * 20),
    INVINCIBILITY(0),
    PREFEAST(2 * 60 * 20),
    POSTFEAST(15 * 60 * 20);

    private int gameStateStart;

    private GameState(int gameStateStart) {
        this.gameStateStart = gameStateStart;
    }

    public int getGameStateStart() {
        return gameStateStart;
    }
}
