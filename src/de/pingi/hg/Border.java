package de.pingi.hg;

public class Border {

    private static int borderSize = 500;
    private static int announcementTime = 25*60*20;
    private static int shrinkTime = 30*60*20;
    private static int borderTaskID;

    public static int getBorderSize() {
        return borderSize;
    }

    public static void setBorderSize(int borderSize) {
        Border.borderSize = borderSize;
    }

    public static int getBorderTaskID() {
        return borderTaskID;
    }

    public static void setBorderTaskID(int borderTaskID) {
        Border.borderTaskID = borderTaskID;
    }

    public static int getAnnouncementTime() {
        return announcementTime;
    }

    public static int getShrinkTime() {
        return shrinkTime;
    }
}
