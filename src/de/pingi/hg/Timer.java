package de.pingi.hg;

import de.pingi.listener.GameTimeEvent;
import de.pingi.main.Main;
import org.bukkit.Bukkit;

public class Timer {

    private static int gameTime = -180 * 20;

    public static void updateGameTime() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {

            @Override
            public void run() {
                gameTime += 20;
                Bukkit.getServer().getPluginManager().callEvent(new GameTimeEvent(Timer.getGameTime(), Bukkit.getServer().getWorld("world")));
            }
            }, 0, 20);
    }

    public static int getGameTime() {
        return gameTime;
    }

    public static void setGameTime(int gameTime) {
        Timer.gameTime = gameTime;
    }

    public static GameState getGameState()
    {
        if (gameTime < GameState.INVINCIBILITY.getGameStateStart()) {
            return GameState.PREGAME;
        }
        else if (gameTime < GameState.PREFEAST.getGameStateStart()) {
            return GameState.INVINCIBILITY;
        }
        else if (gameTime < GameState.POSTFEAST.getGameStateStart()) {
            return GameState.PREFEAST;
        }
        else return GameState.POSTFEAST;
    }
}


