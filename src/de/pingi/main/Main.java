package de.pingi.main;

import de.pingi.commands.*;
import de.pingi.hg.Timer;
import de.pingi.listener.ClickKitListener;
import de.pingi.hg.kit.KitManager;
import de.pingi.listener.HGListener;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.material.CocoaPlant;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Mushroom;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;


public class Main extends JavaPlugin {

    private static Main plugin;
    private PluginManager pluginManager = Bukkit.getPluginManager();

    @Override
    public void onEnable() {
        plugin = this;

        loadKits();

        Timer.updateGameTime();

        getCommand("info").setExecutor(new InfoCommand());
        getCommand("spawn").setExecutor(new SpawnCommand());
        getCommand("setGameTime").setExecutor(new GameTimeCommand());
        getCommand("kills").setExecutor(new KillsCommand());
        getCommand("mainkit").setExecutor(new MainKitCommand());
        getCommand("sidekit").setExecutor(new SideKitCommand());
        getCommand("mainkits").setExecutor(new MainKitsCommand());
        getCommand("sidekits").setExecutor(new SideKitsCommand());

        pluginManager.registerEvents(new HGListener(), this);
        pluginManager.registerEvents(new ClickKitListener(), this);
        //pluginManager.registerEvents(new KitManager(), this);

        ShapelessRecipe cocoaSoup = new ShapelessRecipe(new ItemStack(Material.MUSHROOM_SOUP)).addIngredient(new MaterialData(Material.INK_SACK, (byte) 3)).addIngredient(1, Material.BOWL);
        Bukkit.addRecipe(cocoaSoup);

        ShapelessRecipe cactiSoup = new ShapelessRecipe(new ItemStack(Material.MUSHROOM_SOUP)).addIngredient(2, Material.CACTUS).addIngredient(1, Material.BOWL);
        Bukkit.addRecipe(cactiSoup);

    }

    public void loadKits() {
        ArrayList<Listener> listeners = new ArrayList<Listener>(KitManager.getKits());
        for (Listener l : listeners) {
            pluginManager.registerEvents(l, this);
        }
    }


    public static Main getPlugin() {
        return plugin;
    }
}
