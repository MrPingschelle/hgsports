package de.pingi.commands;

import de.pingi.hg.GameState;
import de.pingi.hg.Timer;
import de.pingi.util.ColorUtil;
import de.pingi.util.Math;
import de.pingi.util.MessageType;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class SpawnCommand extends HGCommand {

    @Override
    public void executeCommand(Player player, String[] args) {
        if (Timer.getGameState() == GameState.PREGAME) {
            int x = Math.randomNumberInRange(-450, 450);
            int z = Math.randomNumberInRange(-450, 450);
            Location location = new Location(player.getWorld(), x, player.getWorld().getHighestBlockYAt(x, z), z);
            player.teleport(location);
        }
        else {
            player.sendMessage(ColorUtil.encolor("You may not use this command during the game", MessageType.BAD));
        }
    }

}
