package de.pingi.commands;

import de.pingi.hg.Timer;
import org.bukkit.entity.Player;

public class GameTimeCommand extends HGCommand {

    @Override
    public void executeCommand(Player player, String[] args) {
        if (args.length == 1) {
            int number = Integer.parseInt(args[0]);
            Timer.setGameTime(number);
        }
    }
}
