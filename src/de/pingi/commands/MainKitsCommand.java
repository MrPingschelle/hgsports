package de.pingi.commands;

import de.pingi.hg.kit.Kit;
import de.pingi.hg.kit.KitManager;
import de.pingi.util.ColorUtil;
import de.pingi.util.MessageType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MainKitsCommand extends HGCommand {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return super.onCommand(sender, command, label, args);
    }

    @Override
    public void executeCommand(Player player, String[] args) {
        String mainKits = "Main Kits: ";
        for (Kit mainKit : KitManager.getMainKits()) {
            mainKits = mainKits + mainKit.getName() + "," + " ";
        }
        player.sendMessage(ColorUtil.encolor(mainKits, MessageType.INFO));
    }
}
