package de.pingi.commands;

import de.pingi.hg.PlayerManager;
import de.pingi.util.ColorUtil;
import de.pingi.util.MessageType;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class KillsCommand extends HGCommand {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return super.onCommand(sender, command, label, args);
    }

    @Override
    public void executeCommand(Player player, String[] args) {
        player.sendMessage(ColorUtil.accentuate("Killed players:", "", MessageType.INFO));
        if (PlayerManager.getKilledPlayers().containsKey(player)) {
            for (Player p : PlayerManager.getKilledPlayers().get(player)) {
                player.sendMessage(ColorUtil.encolor("" + p.getDisplayName(), MessageType.INFO));
            }
        }
    }
}
