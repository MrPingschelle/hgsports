package de.pingi.commands;

import de.pingi.hg.GameState;
import de.pingi.hg.PlayerManager;
import de.pingi.hg.Timer;
import de.pingi.hg.kit.Kit;
import de.pingi.hg.kit.KitManager;
import de.pingi.util.ColorUtil;
import de.pingi.util.MessageType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MainKitCommand extends HGCommand {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return super.onCommand(sender, command, label, args);
    }

    @Override
    public void executeCommand(Player player, String[] args) {
        String string = args[0];
        if (Timer.getGameState() == GameState.PREGAME || Timer.getGameState() == GameState.INVINCIBILITY) {
            for (Kit kit : KitManager.getKits()) {
                if (string.equalsIgnoreCase(kit.getName())) {
                    PlayerManager.getMainKits().put(player, kit);
                    player.sendMessage(ColorUtil.encolor("You have selected ", MessageType.GOOD) + ColorUtil.accentuate(kit.getName() + " ", "as your Main Kit.", MessageType.GOOD));
                }
            }
        }
    }
}
