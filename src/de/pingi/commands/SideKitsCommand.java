package de.pingi.commands;

import de.pingi.hg.kit.Kit;
import de.pingi.hg.kit.KitManager;
import de.pingi.util.ColorUtil;
import de.pingi.util.MessageType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SideKitsCommand extends HGCommand {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return super.onCommand(sender, command, label, args);
    }

    @Override
    public void executeCommand(Player player, String[] args) {
        String sideKits = "Side Kits: ";
        for (Kit sideKit : KitManager.getSideKits()) {
            sideKits = sideKits + sideKit.getName() + "," + " ";
        }
        player.sendMessage(ColorUtil.encolor(sideKits, MessageType.INFO));
    }
}
