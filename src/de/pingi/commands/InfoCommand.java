package de.pingi.commands;

import de.pingi.hg.PlayerManager;
import de.pingi.hg.Timer;
import de.pingi.hg.kit.None;
import de.pingi.main.Main;
import de.pingi.util.ColorUtil;
import de.pingi.util.Math;
import de.pingi.util.MessageType;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;


public class InfoCommand extends HGCommand {

    @Override
    public void executeCommand(Player player, String[] args) {
        player.sendMessage(ColorUtil.accentuate("Game time: ", ""  + Math.convertToTime(Timer.getGameTime()), MessageType.INFO));
        player.sendMessage(ColorUtil.accentuate("Player count: ", ""  + Bukkit.getOnlinePlayers().length, MessageType.INFO));
        player.sendMessage(ColorUtil.accentuate("Main Kit: ", ""  + PlayerManager.getMainKits().getOrDefault(player, new None()).getName(), MessageType.INFO));
        player.sendMessage(ColorUtil.accentuate("Side Kit: ", ""  + PlayerManager.getSideKits().getOrDefault(player, new None()).getName(), MessageType.INFO));

        int kills = PlayerManager.getKills().getOrDefault(player, 0);
        player.sendMessage(ColorUtil.accentuate("Kills: ", ""  + kills, MessageType.INFO));

        player.sendMessage(ColorUtil.accentuate("" + Timer.getGameState(), "", MessageType.INFO));
    }
}
