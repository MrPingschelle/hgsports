package de.pingi.util;

import java.util.Random;

public class Math {

    public static int randomNumberInRange(int min, int max) {
        if (min > max) {
            throw new IllegalArgumentException("max must be greater than min");
        }
        if (min == max) {
            return min;
        }
        else {
            Random r = new Random();
            return r.nextInt((max - min) + 1) + min;
        }
    }

    public static String convertToTime(int number) {
        String prePost = "";
        if (number < 0) {
            prePost = "until the game starts";
        }
        int secondsAmount = java.lang.Math.abs(number) / 20;
        int minutesAmount = (secondsAmount - (secondsAmount % 60)) / 60;
        secondsAmount = secondsAmount % 60;
        int sec1 = (secondsAmount - (secondsAmount % 10)) / 10;
        int sec2 = secondsAmount % 10;
        return minutesAmount + ":" + sec1 + sec2 + " " + prePost;
    }

}
