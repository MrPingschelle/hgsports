package de.pingi.util;

import org.bukkit.ChatColor;

public enum MessageType {

    INFO(ChatColor.YELLOW, ChatColor.GOLD),
    ANNOUNCEMENT(ChatColor.RED, ChatColor.DARK_RED),
    GOOD(ChatColor.GREEN, ChatColor.DARK_GREEN),
    BAD(ChatColor.RED, ChatColor.DARK_RED),
    NEUTRAL(ChatColor.GRAY, ChatColor.WHITE);

    private ChatColor mainColor;
    private ChatColor accentColor;

    MessageType(ChatColor mainColor, ChatColor accentColor) {
        this.mainColor = mainColor;
        this.accentColor = accentColor;
    }

    public ChatColor getMainColor() {
        return mainColor;
    }

    public ChatColor getAccentColor() {
        return accentColor;
    }
}
