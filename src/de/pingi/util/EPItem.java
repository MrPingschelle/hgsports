package de.pingi.util;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum EPItem {

    BROWN_MUSHROOM(new ItemStack(Material.BROWN_MUSHROOM), (double) 1/64),
    RED_MUSHROOM(new ItemStack(Material.RED_MUSHROOM), (double) 1/64),
    CACTUS(new ItemStack(Material.CACTUS), (double) 1/64),
    MUSHROOM_SOUP(new ItemStack(Material.MUSHROOM_SOUP), (double) 1/32),
    WOOD_SWORD(new ItemStack(Material.WOOD_SWORD), 1),
    STONE_SWORD(new ItemStack(Material.STONE_SWORD), 2),
    IRON_SWORD(new ItemStack(Material.IRON_SWORD), 3),
    DIAMOND_SWORD(new ItemStack(Material.DIAMOND_SWORD), 4),
    IRON_INGOT(new ItemStack(Material.IRON_INGOT), (double) 1/8);


    private ItemStack item;
    private double epValue;

    EPItem(ItemStack item, double epValue) {
        this.item = item;
        this.epValue = epValue;
    }

    public ItemStack getItem() {
        return item;
    }

    public double getEpValue() {
        return epValue;
    }
}
