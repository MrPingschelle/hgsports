package de.pingi.util;

import de.pingi.hg.feast.Feast;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

public class StructuresUtil {


    public static void spawnCylinder(Location location, int r, int h, Material m) {
        World world = location.getWorld();
        double x = location.getX();
        double y = location.getY();
        double z = location.getZ();
        for (int i = 1; i <= h; i++) {
            location = new Location(world, x, y, z);
            spawnDisc(location, r, m);
            y += 1;
        }
    }

    public static void spawnDisc(Location location, int r, Material m) {
        for (int i = 1; i <= r; i++) {
            spawnCircle(location, i, m);
        }
    }

    public static void spawnCircle(Location loc, int r, Material m) {
        int x;
        int y = loc.getBlockY();
        int z;

        for (double i = 0.0; i < 360.0; i += 0.1) {
            double angle = i * java.lang.Math.PI / 180;
            x = (int) (loc.getX() + r * java.lang.Math.cos(angle));
            z = (int) (loc.getZ() + r * java.lang.Math.sin(angle));

            Bukkit.getServer().getWorld("world").getBlockAt(x, y, z).setType(m);
            Feast.getFeastBlocks().add(Bukkit.getServer().getWorld("world").getBlockAt(x, y, z));
        }
    }

    public static void spawnCuboid(Block block1, Block block2, Material material) {
        for (int k = block1.getY(); k <= block2.getY(); k++) {
            for (int j = block1.getZ(); j <= block2.getZ(); j++) {
                for (int i = block1.getX(); i <= block2.getX(); i++) {
                    Bukkit.getWorld("world").getBlockAt(i, k, j).setType(material);
                }
            }
        }
    }

}
