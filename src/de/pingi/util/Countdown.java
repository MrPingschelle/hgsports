package de.pingi.util;

import de.pingi.hg.Timer;
import de.pingi.main.Main;
import org.bukkit.Bukkit;

public class Countdown {

    private int countdownTaskID = 0;

    public Countdown(String message1, String message2, int from, int to, String endMessage) {
        this.createCountdown(message1, message2, from, to, endMessage);
    }

    public void createCountdown(String message1, String message2, int from, int to, String endMessage) {
            countdownTaskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {
                @Override
                public void run() {
                    int timeLeft = to - Timer.getGameTime();
                    if (timeLeft > 0 && timeLeft <= to - from && (timeLeft % (60*20) == 0|| timeLeft == 30*20 || timeLeft == 15*20 || timeLeft == 10*20 || timeLeft == 9*20 || timeLeft == 8*20 || timeLeft == 7*20 || timeLeft == 6*20 || timeLeft == 5*20 || timeLeft == 4*20 || timeLeft == 3*20 || timeLeft == 2*20 || timeLeft == 1*20)) {
                        Bukkit.broadcastMessage(ColorUtil.accentuate(message1 + Math.convertToTime(timeLeft) + "minutes.", message2, MessageType.INFO));
                    }
                    if (timeLeft == 0) {
                        Bukkit.broadcastMessage(ColorUtil.accentuate(endMessage, "", MessageType.INFO));
                    }
                    if (timeLeft <= 0) {
                        Bukkit.getScheduler().cancelTask(countdownTaskID);
                    }
                }
            }, 0, 1*20);
    }
}
