package de.pingi.util;

public class ColorUtil {

    public static String encolor(String message, MessageType messageType) {
        return messageType.getMainColor() + message;
    }

    public static String accentuate(String message1, String message2, MessageType messageType) {
        return messageType.getAccentColor() + message1 + messageType.getMainColor() + message2;
    }
}
