package de.pingi.util;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class EquipUtil {

    public static double calcEP(Player player) {
        double ep = 0;
        for (ItemStack itemStack : player.getInventory().getContents()) {
            for (EPItem epItem : EPItem.values()) {
                if (itemStack == epItem.getItem()) {
                    ep += epItem.getEpValue();
                }
            }
        }
        for (ItemStack itemStack : player.getEquipment().getArmorContents()) {
            if (itemStack.getType() == Material.LEATHER_HELMET || itemStack.getType() == Material.LEATHER_BOOTS || itemStack.getType() == Material.GOLD_BOOTS || itemStack.getType() == Material.CHAINMAIL_BOOTS) {
                ep += 0.5;
            }
            if (itemStack.getType() == Material.LEATHER_LEGGINGS || itemStack.getType() == Material.GOLD_HELMET || itemStack.getType() == Material.CHAINMAIL_HELMET || itemStack.getType() == Material.IRON_HELMET || itemStack.getType() == Material.IRON_BOOTS) {
                ep += 1;
            }
            if (itemStack.getType() == Material.LEATHER_CHESTPLATE || itemStack.getType() == Material.GOLD_LEGGINGS || itemStack.getType() == Material.DIAMOND_HELMET || itemStack.getType() == Material.DIAMOND_BOOTS) {
                ep += 1.5;
            }
            if (itemStack.getType() == Material.CHAINMAIL_LEGGINGS) {
                ep += 2;
            }
            if (itemStack.getType() == Material.CHAINMAIL_CHESTPLATE ||itemStack.getType() == Material.IRON_LEGGINGS) {
                ep += 2.5;
            }
            if (itemStack.getType() == Material.IRON_CHESTPLATE || itemStack.getType() == Material.DIAMOND_LEGGINGS) {
                ep += 3;
            }
            if (itemStack.getType() == Material.DIAMOND_CHESTPLATE) {
                ep += 4;
            }
        }
        //for (ItemStack itemStack : player.getInventory().getContents()) {
        //    if (itemStack.getType() == Material.LEATHER_HELMET || itemStack.getType() == Material.LEATHER_BOOTS || itemStack.getType() == Material.GOLD_BOOTS || itemStack.getType() == Material.CHAINMAIL_BOOTS) {
        //        ep += 0.5;
        //    }
        //    if (itemStack.getType() == Material.LEATHER_LEGGINGS || itemStack.getType() == Material.GOLD_HELMET || itemStack.getType() == Material.CHAINMAIL_HELMET || itemStack.getType() == Material.IRON_HELMET || itemStack.getType() == Material.IRON_BOOTS) {
        //        ep += 1;
        //    }
        //    if (itemStack.getType() == Material.LEATHER_CHESTPLATE || itemStack.getType() == Material.GOLD_LEGGINGS || itemStack.getType() == Material.DIAMOND_HELMET || itemStack.getType() == Material.DIAMOND_BOOTS) {
        //        ep += 1.5;
        //    }
        //    if (itemStack.getType() == Material.CHAINMAIL_LEGGINGS) {
        //        ep += 2;
        //    }
        //    if (itemStack.getType() == Material.CHAINMAIL_CHESTPLATE ||itemStack.getType() == Material.IRON_LEGGINGS) {
        //        ep += 2.5;
        //    }
        //    if (itemStack.getType() == Material.IRON_CHESTPLATE || itemStack.getType() == Material.DIAMOND_LEGGINGS) {
        //        ep += 3;
        //    }
        //    if (itemStack.getType() == Material.DIAMOND_CHESTPLATE) {
        //        ep += 4;
        //    }
        //}
        return ep;
    }

}
