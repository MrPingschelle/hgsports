package de.pingi.util;

import de.pingi.hg.feast.Feast;
import de.pingi.hg.feast.FeastItem;
import de.pingi.hg.lootdrop.LootDrop;
import de.pingi.hg.lootdrop.LootDropItem;
import org.bukkit.block.Chest;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;


public class LootUtil {

    private static List<Integer> lootDropTaskIDs = new ArrayList<Integer>();

    public static void fillUpFeastChest(Chest chest) {
        for (int j = 0; j <= 26; j++) {
            fillUpFeastSlot(chest, j);
        }
    }

    public static void fillUpFeastSlot(Chest chest, int slot) {
        int randomNumber = Math.randomNumberInRange(1, 324);
        FeastItem chosenItem;
        int currentItemNumber;
        int previousItemNumber = 0;
        for (int i = 0; i < Feast.getFeastItems().size(); i++) {
            currentItemNumber = getFeastNumber(Feast.getFeastItems().get(i));
            if (currentItemNumber >= randomNumber && previousItemNumber < randomNumber) {
                chosenItem = Feast.getFeastItems().get(i);
                chest.getBlockInventory().setItem(slot, new ItemStack(chosenItem.getItem(), Math.randomNumberInRange(chosenItem.getMinAmount(), chosenItem.getMaxAmount())));
                break;
            }
            previousItemNumber = currentItemNumber;
        }
    }

    public static int getFeastNumber(FeastItem feastItem) {
        int number = 0;
        for (FeastItem f : Feast.getFeastItems()) {
            number = (int) number + (int) f.getProbability();
            if (f == feastItem) {
                break;
            }
        }
        return number;
    }

    public static void fillUpLootDrop(Chest chest) {
        for (int j = 0; j <= 26; j++) {
            fillUpLootDropSlot(chest, j);
        }
    }

    public static void fillUpLootDropSlot(Chest chest, int slot) {
        int randomNumber = Math.randomNumberInRange(1, 324);
        LootDropItem chosenItem;
        int currentItemNumber;
        int previousItemNumber = 0;
        for (int i = 0; i < LootDrop.getLootDropItems().size(); i++) {
            currentItemNumber = getLootDropNumber(LootDrop.getLootDropItems().get(i));
            if (currentItemNumber >= randomNumber && previousItemNumber < randomNumber) {
                chosenItem = LootDrop.getLootDropItems().get(i);
                chest.getBlockInventory().setItem(slot, new ItemStack(chosenItem.getItem(), Math.randomNumberInRange(chosenItem.getMinAmount(), chosenItem.getMaxAmount())));
                break;
            }
            previousItemNumber = currentItemNumber;
        }
    }

    public static int getLootDropNumber(LootDropItem lootDropItem) {
        int number = 0;
        for (LootDropItem l : LootDrop.getLootDropItems()) {
            number = (int) number + (int) l.getProbability();
            if (l == lootDropItem) {
                break;
            }
        }
        return number;
    }

    public static List<Integer> getLootDropTaskIDs() {
        return lootDropTaskIDs;
    }
}
