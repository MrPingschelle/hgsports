package de.pingi.listener;

import de.pingi.hg.GameState;
import de.pingi.hg.PlayerManager;
import de.pingi.hg.Timer;
import de.pingi.hg.kit.Kit;
import de.pingi.main.Main;
import de.pingi.util.ColorUtil;
import de.pingi.util.Math;
import de.pingi.util.MessageType;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class ClickKitListener implements Listener {

    @EventHandler
    public void onACKPlayerInteract(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        onActivateClickKit(e, player);
    }

    @EventHandler
    public void onACKPlayerDamage(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player) {
            Player player = (Player) e.getDamager();
            onActivateClickKit(e, player);
        }
    }

    public void onActivateClickKit(Event e, Player player) {
        Kit mainKit;
        Kit sideKit;
        if (PlayerManager.getMainKits().containsKey(player)) {
            mainKit = PlayerManager.getMainKits().get(player);
            if (mainKit.getClickItem() != null) {
                if (player.getItemInHand().getType() == mainKit.getClickItem().getType()) {
                    if (mainKit.isUsable(e)) {
                        if (!PlayerManager.getOnMainKitCooldowns().getOrDefault(player, false)) {
                            mainKit.onClickUse(e);
                            PlayerManager.getOnMainKitCooldowns().put(player, true);
                            PlayerManager.getMainKitCooldowns().put(player, mainKit.getCooldown());
                            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
                                @Override
                                public void run() {
                                    PlayerManager.getOnMainKitCooldowns().put(player, false);
                                }
                            }, mainKit.getCooldown());
                            PlayerManager.getMainKitTaskIDs().put(player, Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {
                                @Override
                                public void run() {
                                    PlayerManager.getMainKitCooldowns().put(player, PlayerManager.getMainKitCooldowns().getOrDefault(player, mainKit.getCooldown()) - 20);
                                    if (PlayerManager.getMainKitCooldowns().getOrDefault(player, mainKit.getCooldown()) == 0) {
                                        Bukkit.getScheduler().cancelTask(PlayerManager.getMainKitTaskIDs().get(player));
                                    }
                                }
                            }, 0, 1 * 20));
                        } else {
                            player.sendMessage(ColorUtil.encolor("You are still on cooldown for " + Math.convertToTime(PlayerManager.getMainKitCooldowns().getOrDefault(player, 0)), MessageType.BAD) + "minutes");
                        }
                    }
                }
            }
        }
        if (PlayerManager.getSideKits().containsKey(player)) {
            sideKit = PlayerManager.getSideKits().get(player);
            if (sideKit.getClickItem() != null) {
                if (player.getItemInHand().getType() == sideKit.getClickItem().getType()) {
                    if (sideKit.isUsable(e)) {
                        if (!PlayerManager.getOnSideKitCooldowns().getOrDefault(player, false)) {
                            sideKit.onClickUse(e);
                            PlayerManager.getOnSideKitCooldowns().put(player, true);
                            PlayerManager.getSideKitCooldowns().put(player, sideKit.getCooldown());
                            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
                                @Override
                                public void run() {
                                    PlayerManager.getOnSideKitCooldowns().put(player, false);
                                }
                            }, sideKit.getCooldown());
                            PlayerManager.getSideKitTaskIDs().put(player, Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {
                                @Override
                                public void run() {
                                    PlayerManager.getSideKitCooldowns().put(player, PlayerManager.getSideKitCooldowns().getOrDefault(player, sideKit.getCooldown()) - 20);
                                    if (PlayerManager.getSideKitCooldowns().getOrDefault(player, sideKit.getCooldown()) == 0) {
                                        Bukkit.getScheduler().cancelTask(PlayerManager.getSideKitTaskIDs().get(player));
                                    }
                                }
                            }, 0, 1 * 20));
                        } else {
                            player.sendMessage(ColorUtil.encolor("You are still on cooldown for " + Math.convertToTime(PlayerManager.getSideKitCooldowns().getOrDefault(player, 0)), MessageType.BAD) + "minutes");
                        }
                    }
                }
            }
        }
    }

}


