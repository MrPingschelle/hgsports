package de.pingi.listener;
import de.pingi.hg.*;
import de.pingi.hg.Timer;
import de.pingi.hg.feast.Feast;
import de.pingi.hg.kit.Kit;
import de.pingi.hg.kit.KitManager;
import de.pingi.main.Main;
import de.pingi.util.*;
import de.pingi.util.Math;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.util.Vector;

import java.util.*;

public class HGListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        if (Timer.getGameState() == GameState.PREGAME) {
            e.getPlayer().getInventory().clear();
            //e.getPlayer().getInventory().setItem(1, new ItemStack(Material.CHEST));
            //e.getPlayer().getInventory().setItem(2, new ItemStack(Material.ENDER_CHEST));
            e.setJoinMessage(ColorUtil.accentuate(e.getPlayer().getDisplayName(), " joined the game", MessageType.NEUTRAL));
            e.getPlayer().teleport(new Location(e.getPlayer().getWorld(), 0, e.getPlayer().getWorld().getHighestBlockYAt(0, 0), 0));
            e.getPlayer().setFoodLevel(20);
        }
        else {
            if (!PlayerManager.getAliveOrDeadPlayers().containsKey(e.getPlayer()) || !PlayerManager.getAliveOrDeadPlayers().get(e.getPlayer())) {
                e.getPlayer().kickPlayer(ColorUtil.encolor("The game has already started", MessageType.INFO));
            }
            if (!PlayerManager.getAliveOrDeadPlayers().get(e.getPlayer())) {
                e.getPlayer().kickPlayer(ColorUtil.encolor("You are dead", MessageType.INFO));
            }
        }
    }

    @EventHandler
    public void onCompassUse(PlayerInteractEvent e) {
        if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (e.getPlayer().getItemInHand().getType() == Material.COMPASS) {

                HashMap<Player, Double> distances = new HashMap<Player, Double>();

                for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                    double distance = e.getPlayer().getLocation().distance(player.getLocation());
                    if (player != e.getPlayer()) {
                        distances.put(player, distance);
                    }
                }

                Map.Entry<Player, Double> minEntry = null;

                for (Map.Entry<Player, Double> entry : distances.entrySet())
                {
                    assert false;
                    if (minEntry == null  || minEntry.getValue() > entry.getValue())
                    {
                        minEntry = entry;
                    }
                }

                assert false;
                Player closestPlayer = minEntry.getKey();

                PlayerManager.getClosestPlayers().put(e.getPlayer(), closestPlayer);

                e.getPlayer().sendMessage(ColorUtil.encolor("Your tracker is pointing to ", MessageType.INFO) + ColorUtil.accentuate("" + closestPlayer.getDisplayName(), "", MessageType.INFO));
                if (distances.get(closestPlayer) <= 20) {
                    e.getPlayer().sendMessage(ColorUtil.encolor("He seems to be very close...", MessageType.BAD));
                }

                Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {
                    @Override
                    public void run() {
                        e.getPlayer().setCompassTarget(PlayerManager.getClosestPlayers().get(e.getPlayer()).getLocation());
                    }
                },0,0);
            }
        }
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player && (Timer.getGameState() == GameState.PREGAME || Timer.getGameState() == GameState.INVINCIBILITY)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerBreakBlock(BlockBreakEvent e) {
        if (Timer.getGameState() == GameState.PREGAME) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerPlaceBlock(BlockPlaceEvent e) {
        if (Timer.getGameState() == GameState.PREGAME) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerSoup(PlayerInteractEvent e) {
        if (e.getPlayer().getItemInHand().getType() == Material.MUSHROOM_SOUP && (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))) {
            if (e.getPlayer().getHealth() <= 13) {
                e.getPlayer().setHealth(e.getPlayer().getHealth() + 7);
                e.getPlayer().getInventory().getItemInHand().setType(Material.BOWL);
            }
            else {
                if (e.getPlayer().getHealth() < 20) {
                    e.getPlayer().setHealth(20);
                    e.getPlayer().getInventory().getItemInHand().setType(Material.BOWL);
                }
                else {
                    if (e.getPlayer().getFoodLevel() <= 13) {
                        e.getPlayer().setFoodLevel((e.getPlayer().getFoodLevel() + 7));
                        e.getPlayer().getInventory().getItemInHand().setType(Material.BOWL);
                    }
                    else if (e.getPlayer().getFoodLevel() < 20) {
                        e.getPlayer().setFoodLevel(20);
                        e.getPlayer().getInventory().getItemInHand().setType(Material.BOWL);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerEatSoup(PlayerItemConsumeEvent e) {
        if (e.getItem().getType() == Material.MUSHROOM_SOUP) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerFoodLevelChange(FoodLevelChangeEvent e) {
        int i = Math.randomNumberInRange(0, 1000);
        if (i < 950) {
            e.setCancelled(true);
        }
        if (Timer.getGameState() == GameState.PREGAME) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerNearBorder(PlayerMoveEvent e) {
        double xFrom = e.getFrom().getX();
        double zFrom = e.getFrom().getZ();
        double xTo = e.getTo().getX();
        double zTo = e.getTo().getZ();
        if ((java.lang.Math.abs(xFrom) <= Border.getBorderSize() - 25 && java.lang.Math.abs(xTo) >= Border.getBorderSize() - 25)
                || (java.lang.Math.abs(zFrom) <= Border.getBorderSize() - 25 && java.lang.Math.abs(zTo) >= Border.getBorderSize() - 25)) {
            e.getPlayer().sendMessage(ColorUtil.encolor("You are close to the forcefield!", MessageType.BAD));
            e.getPlayer().sendMessage(ColorUtil.encolor("You are close to the forcefield!", MessageType.BAD));
            e.getPlayer().sendMessage(ColorUtil.encolor("You are close to the forcefield!", MessageType.BAD));
        }
    }

    @EventHandler
    public void onPlayerSurpassBorder(PlayerMoveEvent e) {
        double xFrom = e.getFrom().getX();
        double zFrom = e.getFrom().getZ();
        double xTo = e.getTo().getX();
        double zTo = e.getTo().getZ();
        if ((java.lang.Math.abs(xFrom) <= Border.getBorderSize() && java.lang.Math.abs(xTo) >= Border.getBorderSize())
                || (java.lang.Math.abs(zFrom) <= Border.getBorderSize()&& java.lang.Math.abs(zTo) >= Border.getBorderSize())) {
            if (Timer.getGameState() != GameState.PREGAME && Timer.getGameState() != GameState.INVINCIBILITY) {
                int taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {
                    @Override
                    public void run() {
                        e.getPlayer().sendMessage(ColorUtil.encolor("You are ", MessageType.BAD) + ColorUtil.accentuate("inside ", "the forcefield!", MessageType.BAD));
                        e.getPlayer().damage(5);
                    }
                }, 0, 20);
                Border.setBorderTaskID(taskID);
            }
            else if (Timer.getGameState() == GameState.INVINCIBILITY) {
                e.getPlayer().teleport(new Location(e.getPlayer().getWorld(), xFrom, e.getFrom().getY(), zTo));
            }
            else if (Timer.getGameState() == GameState.PREGAME) {
                e.getPlayer().teleport(new Location(e.getPlayer().getWorld(), 0, e.getPlayer().getWorld().getHighestBlockYAt(0, 0), 0));
            }
        }
    }

    @EventHandler
    public void onPlayerReturnToBorder(PlayerMoveEvent e) {
        double xFrom = e.getFrom().getX();
        double zFrom = e.getFrom().getZ();
        double xTo = e.getTo().getX();
        double zTo = e.getTo().getZ();
        if ((java.lang.Math.abs(xFrom) >= Border.getBorderSize() && java.lang.Math.abs(xTo) <= Border.getBorderSize())
                || (java.lang.Math.abs(zFrom) >= Border.getBorderSize()&& java.lang.Math.abs(zTo) <= Border.getBorderSize())) {
            Bukkit.getScheduler().cancelTask(Border.getBorderTaskID());
        }
    }

    @EventHandler
    public void onAboutToStart(GameTimeEvent e) {
        if (e.getGameTime() == -15*20){
            Location location;
            for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                int x = Math.randomNumberInRange(-20, 20);
                int z = Math.randomNumberInRange(-20, 20);
                location = new Location(player.getWorld(), x, player.getWorld().getHighestBlockYAt(x, z), z);
                player.teleport(location);
            }
        }
    }

    @EventHandler
    public void onPlayerFreeze(PlayerMoveEvent e) {
        if (Timer.getGameTime() >= -15*20 && Timer.getGameTime() < 0) {
            e.getPlayer().setVelocity(new Vector(0,0,0));
        }
    }

    @EventHandler
    public void onBorderShrink(GameTimeEvent e) {
        if (Timer.getGameTime() == Border.getAnnouncementTime()) {
            new Countdown("The forcefield will will come closer in ", " All players outside 100x100 will be teleported inside the forcefield border.", Border.getAnnouncementTime(), Border.getShrinkTime(), "The forcefield came closer.");
        }
        if (Timer.getGameTime() == Border.getShrinkTime()) {
            Border.setBorderSize(100);
            Location location;
            for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                int x = Math.randomNumberInRange(-75, 75);
                int z = Math.randomNumberInRange(-75, 75);
                location = new Location(player.getWorld(), x, player.getWorld().getHighestBlockYAt(x, z), z);
                if (java.lang.Math.abs(player.getLocation().getX()) > 100 || java.lang.Math.abs(player.getLocation().getZ()) > 100 ) {
                    player.teleport(location);
                }
            }
        }
    }

    @EventHandler
    public void onFeastSpawn(GameTimeEvent e) {
        int x = 0;
        int y = 0;
        int z = 0;
        if (e.getGameTime() == Feast.getAnnouncementTime()) {
            x = Math.randomNumberInRange(-75, 75);
            z = Math.randomNumberInRange(-75, 75);
            y = e.getWorld().getHighestBlockYAt(x, z);
            Location location = new Location(e.getWorld(), x, y, z);
            Feast.setX(x);
            Feast.setY(y);
            Feast.setZ(z);
            StructuresUtil.spawnDisc(location, 15, Material.GRASS);
            StructuresUtil.spawnCylinder(new Location(e.getWorld(), x, y + 1, z), 15, 30, Material.AIR);
            new Countdown("The feast will spawn at " + "x: " + Feast.getX() + ", y: " + Feast.getY() + ", z: " + Feast.getZ() + " in ", "", Feast.getAnnouncementTime(), Feast.getSpawnTime(), "Feast has spawned");
        }
        if (e.getGameTime() == Feast.getSpawnTime()) {
            Feast.setHasSpawned(true);

            Bukkit.getServer().getWorld("world").getBlockAt(Feast.getX(), Feast.getY() + 1, Feast.getZ()).setType(Material.ENCHANTMENT_TABLE);
            Block chest1 = Bukkit.getServer().getWorld("world").getBlockAt(Feast.getX() - 1, Feast.getY() + 1, Feast.getZ() - 1);
            Block chest2 = Bukkit.getServer().getWorld("world").getBlockAt(Feast.getX() + 1, Feast.getY() + 1, Feast.getZ() + 1);
            Block chest3 = Bukkit.getServer().getWorld("world").getBlockAt(Feast.getX() - 1, Feast.getY() + 1, Feast.getZ() + 1);
            Block chest4 = Bukkit.getServer().getWorld("world").getBlockAt(Feast.getX() + 1, Feast.getY() + 1, Feast.getZ() - 1);
            Block chest5 = Bukkit.getServer().getWorld("world").getBlockAt(Feast.getX() - 2, Feast.getY() + 1, Feast.getZ() - 2);
            Block chest6 = Bukkit.getServer().getWorld("world").getBlockAt(Feast.getX() + 2, Feast.getY() + 1, Feast.getZ() + 2);
            Block chest7 = Bukkit.getServer().getWorld("world").getBlockAt(Feast.getX() - 2, Feast.getY() + 1, Feast.getZ() + 2);
            Block chest8 = Bukkit.getServer().getWorld("world").getBlockAt(Feast.getX() + 2, Feast.getY() + 1, Feast.getZ() - 2);
            Block chest9 = Bukkit.getServer().getWorld("world").getBlockAt(Feast.getX(), Feast.getY() + 1, Feast.getZ() - 2);
            Block chest10 = Bukkit.getServer().getWorld("world").getBlockAt(Feast.getX(), Feast.getY() + 1, Feast.getZ() + 2);
            Block chest11 = Bukkit.getServer().getWorld("world").getBlockAt(Feast.getX() - 2, Feast.getY() + 1, Feast.getZ());
            Block chest12 = Bukkit.getServer().getWorld("world").getBlockAt(Feast.getX() + 2, Feast.getY() + 1, Feast.getZ());

            chest1.setType(Material.CHEST);
            chest2.setType(Material.CHEST);
            chest3.setType(Material.CHEST);
            chest4.setType(Material.CHEST);
            chest5.setType(Material.CHEST);
            chest6.setType(Material.CHEST);
            chest7.setType(Material.CHEST);
            chest8.setType(Material.CHEST);
            chest9.setType(Material.CHEST);
            chest10.setType(Material.CHEST);
            chest11.setType(Material.CHEST);
            chest12.setType(Material.CHEST);

            LootUtil.fillUpFeastChest((Chest)chest1.getState());
            LootUtil.fillUpFeastChest((Chest)chest2.getState());
            LootUtil.fillUpFeastChest((Chest)chest3.getState());
            LootUtil.fillUpFeastChest((Chest)chest4.getState());
            LootUtil.fillUpFeastChest((Chest)chest5.getState());
            LootUtil.fillUpFeastChest((Chest)chest6.getState());
            LootUtil.fillUpFeastChest((Chest)chest7.getState());
            LootUtil.fillUpFeastChest((Chest)chest8.getState());
            LootUtil.fillUpFeastChest((Chest)chest9.getState());
            LootUtil.fillUpFeastChest((Chest)chest10.getState());
            LootUtil.fillUpFeastChest((Chest)chest11.getState());
            LootUtil.fillUpFeastChest((Chest)chest12.getState());
        }
    }

    @EventHandler
    public void onFeastBlockBreak(BlockBreakEvent e) {
        if (Feast.getFeastBlocks().contains(e.getBlock()) && !Feast.hasSpawned()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onFeastBlockPlace(BlockPlaceEvent e) {
        if (Feast.getFeastBlocks().contains(e.getBlock()) && !Feast.hasSpawned()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onGameStart(GameTimeEvent e) {
        if (Timer.getGameTime() == GameState.PREGAME.getGameStateStart() + 60*20) {
            new Countdown("The game starts in ", "", GameState.PREGAME.getGameStateStart() + 60 * 20, GameState.INVINCIBILITY.getGameStateStart(), "The game has started");
        }
        if (Timer.getGameTime() == GameState.INVINCIBILITY.getGameStateStart()) {
            new Countdown("Invincibility wears off in ", "", GameState.INVINCIBILITY.getGameStateStart(), GameState.PREFEAST.getGameStateStart(), "Invincibility has worn off");
            for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                PlayerManager.getAliveOrDeadPlayers().put(player, true);
                player.getInventory().clear();
                player.getInventory().setItem(8, new ItemStack(Material.COMPASS));
                if (PlayerManager.getMainKits().containsKey(player)) {
                    Kit kit = PlayerManager.getMainKits().get(player);
                    kit.equip(player, 0);
                }
                if (PlayerManager.getSideKits().containsKey(player)) {
                    Kit kit = PlayerManager.getSideKits().get(player);
                    kit.equip(player, 1);
                }
            }
        }
    }

    @EventHandler (priority = EventPriority.HIGH)
    public void onLootRain(GameTimeEvent e) {
        if (Timer.getGameTime() == 35*60*20) {
            Bukkit.broadcastMessage(ColorUtil.accentuate("Loot drops are falling off the sky inside 50x50! ", "Watch out for them to upgrade your gear!", MessageType.INFO));
            for (int i = 1; i <= 12; i++) {
                int x = Math.randomNumberInRange(-50, 50);
                int z = Math.randomNumberInRange(-50, 50);
                byte chiffre = 0;
                FallingBlock lootDrop = e.getWorld().spawnFallingBlock(new Location(e.getWorld(), x, 140, z), Material.PISTON_BASE, chiffre);
                int taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {
                    @Override
                    public void run() {
                        lootDrop.setVelocity(new Vector(0, -0.1, 0));
                    }
                },0,0);
                LootUtil.getLootDropTaskIDs().add(taskID);
            }
        }
    }

    @EventHandler
    public void onLootDropped(EntityChangeBlockEvent e) {
        if (e.getEntity() instanceof FallingBlock) {
            FallingBlock fallingBlock = (FallingBlock) e.getEntity();
            if (fallingBlock.getMaterial() == Material.PISTON_BASE) {
                e.setCancelled(true);
                Block landingBlock = e.getBlock();
                landingBlock.setType(Material.CHEST);
                Chest chest = (Chest) landingBlock.getState();
                LootUtil.fillUpLootDrop(chest);
                for (Integer taskID : LootUtil.getLootDropTaskIDs()) {
                    Bukkit.getScheduler().cancelTask(taskID);
                }
            }
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        e.getEntity().kickPlayer(ColorUtil.encolor("You died", MessageType.BAD));
        PlayerManager.getAliveOrDeadPlayers().put(e.getEntity(), false);
        if (e.getEntity().getKiller() != null) {
            if (PlayerManager.getKilledPlayers().containsKey(e.getEntity().getKiller())) {
                ArrayList<Player> newKilledPlayers = PlayerManager.getKilledPlayers().get(e.getEntity().getKiller());
                newKilledPlayers.add(e.getEntity());
                PlayerManager.getKilledPlayers().put(e.getEntity().getKiller(), newKilledPlayers);
            }
            else {
                PlayerManager.getKilledPlayers().put(e.getEntity().getKiller(), new ArrayList<Player>(Collections.singletonList(e.getEntity())));
            }
            PlayerManager.getKills().put(e.getEntity().getKiller(), PlayerManager.getKilledPlayers().get(e.getEntity().getKiller()).size());
            if (Timer.getGameState() == GameState.PREFEAST) {
                e.getEntity().getKiller().getInventory().addItem(new ItemStack(Material.BROWN_MUSHROOM, 32));
                e.getEntity().getKiller().getInventory().addItem(new ItemStack(Material.RED_MUSHROOM, 32));
            }
            else {
                e.getEntity().getKiller().getInventory().addItem(new ItemStack(Material.DIAMOND, 4));
            }
        }
        int alivePlayers = 0;
        Player lastPlayer = null;
        for (Map.Entry<Player, Boolean> entry : PlayerManager.getAliveOrDeadPlayers().entrySet()) {
            if (entry.getValue()) {
                alivePlayers += 1;
                lastPlayer = entry.getKey();
            }
        }
        if (alivePlayers == 1) {
            Bukkit.broadcastMessage(ColorUtil.accentuate("Congratulations " + lastPlayer.getDisplayName() + "! ", "You won a round of hardcore games!", MessageType.GOOD));
        }
    }

    @EventHandler
    public void onPlayerKitItemDrop(PlayerDropItemEvent e) {
        if (PlayerManager.getMainKits().containsKey(e.getPlayer())) {
            Kit mainKit = PlayerManager.getMainKits().get(e.getPlayer());
            if (e.getItemDrop().getItemStack().equals(mainKit.getClickItem())) {
                e.setCancelled(true);
            }
        }
        if (PlayerManager.getSideKits().containsKey(e.getPlayer())) {
            Kit sideKit = PlayerManager.getSideKits().get(e.getPlayer());
            if (e.getItemDrop().getItemStack().equals(sideKit.getClickItem())) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlayerKitItemPlace(BlockPlaceEvent e) {
        if (PlayerManager.getMainKits().containsKey(e.getPlayer())) {
            Kit mainKit = PlayerManager.getMainKits().get(e.getPlayer());
            if (mainKit.getClickItem().getType().isBlock()) {
                if (e.getBlockPlaced().getType().name().equals(mainKit.getClickItem().getItemMeta().getDisplayName())) {
                    e.setCancelled(true);
                }
            }
        }
        if (PlayerManager.getSideKits().containsKey(e.getPlayer())) {
            Kit sideKit = PlayerManager.getSideKits().get(e.getPlayer());
            if (sideKit.getClickItem().getType().isBlock()) {
                if (e.getBlockPlaced().getType().name().equals(sideKit.getClickItem().getItemMeta().getDisplayName())) {
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onFight(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player) {
            if (e.getEntity() instanceof Player) {
                if (e.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_ATTACK)) {
                    e.setDamage(e.getFinalDamage() * 0.5);
                }
            }
        }
    }

}
