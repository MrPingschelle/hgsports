package de.pingi.listener;

import org.bukkit.World;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Event;

public class GameTimeEvent extends Event {

    private int gameTime;
    private World world;

    private static final HandlerList handlers = new HandlerList();

    public GameTimeEvent(int gameTime, World world) {
        this.gameTime = gameTime;
        this.world = world;
    }

    public int getGameTime() {
        return gameTime;
    }

    public World getWorld() {
        return world;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
